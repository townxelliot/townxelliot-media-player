var shell = require('shelljs');

// this must also be set in xwalk-audiofs-extension-src/build.xml
var CROSSWALK_VERSION = '8.36.174.0';

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-requirejs');
  grunt.loadNpmTasks('grunt-mochaccino');
  grunt.loadNpmTasks('grunt-simple_server');
  grunt.loadTasks('./tools/grunt-tasks');

  grunt.initConfig({
    clean: [
      'build'
    ],

    // minify and concat CSS
    cssmin: {
      all: {
        files: {
          'build/app/assets/base.css': [
            'app/assets/base.css'
          ]
        }
      }
    },

    // minify main HTML page and HTML templates
    htmlmin: {
      all: {
        files: [
          { src: 'app/index.html', dest: 'build/app/index.html' },
          { cwd: 'app/templates', expand: true, src: '*', dest: 'build/app/templates/' }
        ],
        options: {
          removeComments: true,
          collapseWhitespace: true,
          removeCommentsFromCDATA: false,
          removeCDATASectionsFromCDATA: false,
          removeEmptyAttributes: true,
          removeEmptyElements: false
        }
      }
    },

    uglify: {
      all: {
        files: [
          { src: 'app/lib/requirejs/require.js', dest: 'build/app/js/require.min.js' }
        ]
      }
    },

    // replace stylesheet and js elements in index.html
    condense: {
      all: {
        file: 'build/app/index.html',
        script: {
          src: 'js/require.min.js',
          attrs: {
            'data-main': 'js/main.min'
          }
        }
      }
    },

    // uglify the main module (and its dependency graph)
    // and copies it to build/app/main.min.js with almond integrated
    // into it
    requirejs: {
      all: {
        options: {
          baseUrl: './app/js',

          // see notes in that file on the allowed format
          mainConfigFile: './app/js/amd-config.js',

          include: ['audio-fs-stub', 'xwalk-audio-fs', 'main'],

          // output
          out: 'build/app/js/main.min.js',

          // we don't need to wrap the js in an anonymous function
          wrap: false,

          // keep license comments in js files?
          preserveLicenseComments: false,

          uglify: {
            beautify: false,
            toplevel: true,
            ascii_only: true,
            no_mangle: false,
            max_line_length: 1000
          }
        }
      }
    },

    copy: {
      common: {
        files: [
          {
            src: 'app/lib/press-start-font/index.woff',
            dest: 'build/app/lib/press-start-font/index.woff'
          }
        ]
      },

      xwalk: {
        files: [
          {
            src: 'radio.png',
            dest: 'build/app/radio.png'
          },

          {
            src: 'manifest.json',
            dest: 'build/app/manifest.json'
          }
        ]
      }
    },

    mochaccino: {
      unit: {
        files: [
          { src: 'test/unit/*.test.js' }
        ]
      }
    },

    simple_server: {
      dir: '.'
    }
  });

  grunt.registerTask('xwalk_ext', 'Build Crosswalk extension', function () {
    shell.cd('xwalk-audiofs-extension-src');
    shell.exec('ant');
    shell.cd('..');
  });

  grunt.registerTask('xwalk_package', 'Build Crosswalk package', function (done) {
    shell.cd('xwalk-audiofs-extension-src/lib/crosswalk-' + CROSSWALK_VERSION);
    shell.exec('python make_apk.py --manifest=../../../build/app/manifest.json ' +
               '--extensions=../../xwalk-audiofs-extension --enable-remote-debugging ' +
               '--icon=../../../build/app/radio.png --verbose');
    shell.cd('../../..');

    if (!shell.test('-d', 'build')) {
      shell.mkdir('build');
    }

    shell.cp(
      '-f',

      'xwalk-audiofs-extension-src/lib/crosswalk-' +
      CROSSWALK_VERSION +'/*.apk',

      'build'
    );

    grunt.log.ok('APKS GENERATED:');
    var apks = shell.ls('build/*.apk');
    for (var i = 0; i < apks.length; i++) {
      grunt.log.ok(apks[i]);
    }
  });

  grunt.registerTask('test', ['mochaccino:unit']);

  grunt.registerTask('app', [
    'htmlmin',
    'cssmin',
    'condense:all',
    'uglify:all',
    'requirejs:all',
    'copy:common'
  ]);
  grunt.registerTask('server', ['simple_server']);
  grunt.registerTask('xwalk', ['app', 'copy:xwalk', 'xwalk_ext', 'xwalk_package']);
  grunt.registerTask('default', ['clean', 'xwalk']);
};
