require.config({
  baseUrl: './js',

  deps: ['main'],

  paths: {
    'i18n': '../lib/requirejs-i18n/i18n',
    'domReady': '../lib/requirejs-domready/domReady',
    'text': '../lib/requirejs-text/text',
    'tul': '../lib/tul/TUL',
    'q': '../lib/q/q',
    'gator': '../lib/gator/gator',
    'hammer': '../lib/hammerjs/hammer',
    'audio-fs-stub': '../../test/stubs/audio-fs-stub'
  },

  shim: {
    'gator': {
      exports: 'Gator'
    }
  }
});

// this is here as the r.js optimiser uses the first require.config
// call in the file to configure its build (i.e. to minify and concat
// all the JS files used by the app via require); the argument passed
// to that first call must be valid JSON, and the below config is not
var urlArgs = (document.location.href.match(/nocache/) ?
               'bust=' + (new Date()).getTime() :
               '');

require.config({urlArgs: urlArgs});
