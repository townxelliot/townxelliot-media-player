// perform CRUD actions on albums and tracks in the AlbumCollection, in
// response to events on an AudioFs
define(['models/album', 'models/track'], function (Album, Track) {
  var AudioFsBridge = function (deps) {
    if (!(this instanceof AudioFsBridge)) {
      return new AudioFsBridge(deps);
    }

    deps = deps || {};

    if (!deps.albumCollection) {
      throw new Error('AudioFsBridge needs albumCollection');
    }

    if (!deps.audioFs) {
      throw new Error('AudioFsBridge needs audioFs');
    }

    if (!deps.store) {
      throw new Error('AudioFsBridge needs store');
    }

    this.albumCollection = deps.albumCollection;
    this.audioFs = deps.audioFs;
    this.store = deps.store;

    this.audioFs.on('add', this.fileAdded.bind(this));
    this.audioFs.on('remove', this.fileRemoved.bind(this));
    this.audioFs.on('change', this.fileChanged.bind(this));
  };

  AudioFsBridge.prototype.init = function () {
    this.audioFs.init();
  };

  AudioFsBridge.prototype.fileAdded = function (file) {
    var album = Album({
      title: file.meta.album,
      artist: file.meta.artist
    });

    var id = album.get('id');

    // album collection doesn't have the album
    var albumFromCollection;
    if (albumFromCollection = this.albumCollection.get(id)) {
      album = albumFromCollection;
    }
    else {
      this.albumCollection.update(album);
    }

    var track = Track({
      title: file.meta.title,
      artist: file.meta.artist,
      tracknum: file.meta.tracknum,
      url: file.url,
      filename: file.filename,
      album: album
    });

    // decorate with progress data from the store
    track.set('progress', this.store.loadProgress(track));

    // album doesn't have track
    if (!album.get(track.get('id'))) {
      album.updateTrack(track);
    }
  };

  AudioFsBridge.prototype.fileRemoved = function (file) {
    // TODO
  };

  AudioFsBridge.prototype.fileChanged = function (file) {
    // TODO
  };

  return AudioFsBridge;
});
