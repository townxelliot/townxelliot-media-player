// factory for an object representing a collection of audio
// files on a filesystem;
// implementations should implement the init() method, which
// should call notifyAdd() for each file on the filesystem at the
// point of initialisation; the implementation can then call
// the notify*() methods for subsequent changes on the filesystem
define(['tul', 'q'], function (TUL, Q) {
  var AudioFs = function (deps) {
    if (!(this instanceof AudioFs)) {
      return new AudioFs(deps);
    }

    deps = deps || {};

    if (!deps.audioFsImpl) {
      throw new Error('please provide implementation for AudioFs interface');
    }

    TUL.ext(this, deps.audioFsImpl, TUL.Ev);
  };

  // read files and fire 'add' events for each one found
  AudioFs.prototype.init = function () {
    throw new Error('init() not implemented');
  };

  // implementations should call these methods each time a file is
  // added to/removed from/changed on the file system
  AudioFs.prototype.notifyAdd = function (file) {
    this.fire('add', file);
  };

  AudioFs.prototype.notifyRemove = function (file) {
    this.fire('remove', file);
  };

  AudioFs.prototype.notifyChange = function (file) {
    this.fire('change', file);
  };

  AudioFs.UNKNOWN = 'zzzzzz!!!!THIS-APP-DOES-NOT-' +
                    'KNOW-aaaacadsfadsfgdsgegwegwegwe!!!!'

  return AudioFs;
});
