define(['hammer', 'gator', 'tul'], function (Hammer, Gator, TUL) {
  var clamp = function (value, minValue, maxValue) {
    if (isNaN(value)) {
      value = 0
    }
    else if (value < minValue) {
      value = minValue;
    }
    else if (value > maxValue) {
      value = maxValue;
    }

    return value;
  };

  var AudioPlayer = function (elt) {
    if (!(this instanceof AudioPlayer)) {
      return new AudioPlayer(elt);
    }

    var self = this;

    TUL.ext(this, TUL.Ev);

    this.mediaElt = TUL.$('audio', elt);
    this.currentTimeElt = TUL.$('[data-role="currentTime"]', elt);
    this.durationElt = TUL.$('[data-role="duration"]', elt);

    this.sliderElt = TUL.$('[data-role="slider"]', elt);
    this.sliderBarElt = TUL.$('[data-role="slider-bar"]', elt);

    this.playPauseBtn = TUL.$('[data-role="playPause"]', elt);

    Hammer(this.sliderElt).on('tap', function (e) {
      var tapX = e.gesture.center.clientX;
      self.handleSliderTouch(tapX);
    });

    Hammer(this.sliderElt).on('dragleft dragright', function (e) {
      var startX = e.gesture.center.clientX;
      self.handleSliderTouch(startX);
    });

    this.setButtonText();
    this.playPauseBtn.addEventListener('click', this.toggle.bind(this));

    // skip button handler
    Gator(elt).on('click', '[data-role="skip"]', function () {
      var skipBy = parseInt(this.getAttribute('data-skip-by'));
      self.skip(skipBy);
    });

    // go to album button handler
    Gator(elt).on('click', '[data-role="goAlbum"]', function () {
      self.fire('goAlbum');
    });

    // triggered when data is loaded and available;
    // so we're ready to get duration
    this.mediaElt.addEventListener('durationchange', function () {
      self.setDurationText();
      self.fire('duration', self.mediaElt.duration);
    });

    this.mediaElt.addEventListener('loadeddata', function () {
      self.setCurrentTimeText();
      self.setButtonText();
      self.fire('loaded', self.mediaElt.src);
    });

    this.mediaElt.addEventListener('canplay', function () {
      self.fire('canplay', self.mediaElt.src);
    });

    // progress changed; only fire if duration is available
    this.mediaElt.addEventListener('timeupdate', function () {
      self.setSlider(self.mediaElt.currentTime);
      self.setCurrentTimeText();

      if (!isNaN(self.mediaElt.duration)) {
        self.fire('progress', self.mediaElt.currentTime / self.mediaElt.duration * 100);
      }
    });

    // started playing
    this.mediaElt.addEventListener('play', function () {
      self.fire('play');
      self.setButtonText();
    });

    // paused
    this.mediaElt.addEventListener('pause', function () {
      self.fire('pause');
      self.setButtonText();
    });
  };

  // write the duration text into the UI
  AudioPlayer.prototype.setDurationText = function () {
    if (this.mediaElt.duration > 0) {
      this.durationElt.innerHTML = TUL.timefmt(this.mediaElt.duration);
    }
  };

  // write the current time text into the UI
  AudioPlayer.prototype.setCurrentTimeText = function () {
    if (this.mediaElt.currentTime !== undefined) {
      this.currentTimeElt.innerHTML = TUL.timefmt(this.mediaElt.currentTime);
    }
  };

  // change the position in the track based on a touch on the slider
  AudioPlayer.prototype.handleSliderTouch = function (posX) {
    var percentage = posX / this.sliderElt.clientWidth * 100;
    this.setSliderPercentage(percentage);
    this.seekToPercentage(percentage);
    this.setCurrentTimeText();
  };

  // draw the slider to represent the point "position" in the audio file
  AudioPlayer.prototype.setSlider = function (position) {
    if (this.mediaElt.duration) {
      position = clamp(position, 0, this.mediaElt.duration);
      var percentage = position / this.mediaElt.duration * 100;
      this.setSliderPercentage(percentage);
    }
  };

  // draw the slider with "percentage" width; updates slider bar in the UI
  AudioPlayer.prototype.setSliderPercentage = function (percentage) {
    percentage = clamp(percentage, 0, 100);
    this.sliderBarElt.style.width = percentage + '%';
  };

  // go to position "seconds" in the audio file
  // CHANGES AUDIO ELEMENT currentTime
  AudioPlayer.prototype.seekTo = function (position) {
    var upper = this.mediaElt.duration;
    position = clamp(position, 0, upper);
    this.mediaElt.currentTime = position;
  };

  // go to the point "percentage"% through the audio file; does nothing
  // if the audio file doesn't report its duration correctly
  // CHANGES AUDIO ELEMENT currentTime
  AudioPlayer.prototype.seekToPercentage = function (percentage) {
    percentage = clamp(percentage, 0, 100);

    if (this.mediaElt.duration) {
      this.seekTo(this.mediaElt.duration * percentage / 100);
    }
  };

  // skip by "skipBy" seconds
  // CHANGES AUDIO ELEMENT currentTime
  AudioPlayer.prototype.skip = function (skipBy) {
    this.seekTo(this.mediaElt.currentTime + skipBy);
  };

  AudioPlayer.prototype.setButtonText = function () {
    if (this.mediaElt.src) {
      this.playPauseBtn.style.display = 'inline';
      if (this.mediaElt.paused) {
        this.playPauseBtn.innerHTML = '>';
      }
      else {
        this.playPauseBtn.innerHTML = '||';
      }
    }
    else {
      this.playPauseBtn.style.display = 'none';
    }
  };

  AudioPlayer.prototype.load = function (url) {
    this.mediaElt.src = url;
  };

  AudioPlayer.prototype.play = function () {
    if (this.mediaElt.src) {
      this.mediaElt.play();
    }
  };

  AudioPlayer.prototype.pause = function () {
    if (this.mediaElt.src) {
      this.mediaElt.pause();
    }
  };

  AudioPlayer.prototype.toggle = function () {
    if (this.mediaElt.paused) {
      this.play();
    }
    else {
      this.pause();
    }
  };

  return AudioPlayer;
});
