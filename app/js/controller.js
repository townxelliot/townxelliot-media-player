define(
[
  'tul',
  'models/album-collection',
  'views/albums-view',
  'views/album-view',
  'views/player-view'
],
function (
  TUL,
  AlbumCollection,
  AlbumsView,
  AlbumView,
  PlayerView
)
{
  var Controller = function (albumCollection, store) {
    if (!(this instanceof Controller)) {
      return new Controller(albumCollection, store);
    }

    var self = this;

    this.albumCollection = albumCollection;
    this.store = store;

    this.currentlyPlaying = null;

    var albumsViewElt = TUL.$('[data-view="albums"]');
    this.albumsView = AlbumsView(albumsViewElt, albumCollection);

    var albumViewElt = TUL.$('[data-view="album"]');
    this.albumView = AlbumView(albumViewElt);

    var playerViewElt = TUL.$('[data-view="player"]');
    this.playerView = PlayerView(playerViewElt);

    // catch events on the views; any view change also triggers
    // a store save
    this.albumsView.on('action:showAlbum', function (album) {
      self.showAlbum(album);
    });

    this.albumsView.on('action:playLastTrack', function () {
      var lastPlayed = self.store.lastPlayed;

      if (!lastPlayed) {
        return;
      }

      var track = albumCollection.findTrack(lastPlayed);

      if (track) {
        self.playTrack(track);
      }
    });

    this.albumView.on('action:showAlbums', function () {
      self.showAlbums();
    });

    this.albumView.on('action:playTrack', function (track) {
      self.playTrack(track);
    });

    this.playerView.on('progress', function (data) {
      data.track.set('progress', data.progress);
      store.storeProgress(data.track);
    });

    // the >A button on the player shows the album
    this.playerView.on('action:showAlbum', function (album) {
      self.showAlbum(album);
    });

    // when the app unloads (Firefox OS), persist the store
    window.addEventListener('visibilitychange', function () {
      store.save();
    });

    window.addEventListener('beforeunload', function () {
      store.save();
    });
  };

  Controller.prototype.pageChange = function () {
    var self = this;

    // save the track progress etc.
    this.store.save();

    setTimeout(function () {
      // highlight the currently-playing track (if there is one)
      if (self.playerView.currentlyPlaying) {
        self.albumView.highlightPlayingTrack(self.playerView.currentlyPlaying);
      }
    }, 0);
  };

  Controller.prototype.showAlbums = function () {
    this.albumsView.show();
    this.albumView.hide();
    this.pageChange();
  };

  Controller.prototype.showAlbum = function (album) {
    this.albumView.setAlbum(album);
    this.albumsView.hide();
    this.albumView.show().then(this.pageChange.bind(this));
  };

  Controller.prototype.playTrack = function (track) {
    this.playerView.loadTrack(track);

    // set the "short" class on the albumView and albumsView, to
    // limit their height
    this.albumsView.elt.classList.add('short');
    this.albumView.elt.classList.add('short');

    // record the ID of the last played track in the store
    this.store.saveLastPlayed(track);

    this.pageChange();
  };

  return Controller;
});
