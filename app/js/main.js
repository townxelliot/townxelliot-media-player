require(
[
  'controller',
  'models/album-collection',
  'audio-fs',
  'audio-fs-bridge',
  'store',
  'domReady!'
],
function (
  Controller,
  AlbumCollection,
  AudioFs,
  AudioFsBridge,
  Store
) {
  var audioFsModule = 'audio-fs-stub';

  if (/Crosswalk/.test(navigator.userAgent)) {
    audioFsModule = 'xwalk-audio-fs';
  }

  require([audioFsModule], function (audioFsImpl) {
    var audioFs = AudioFs({audioFsImpl: audioFsImpl});

    // model representing the audio files
    var albumCollection = AlbumCollection();

    // globally expose for testing
    window.albumCollection = albumCollection;

    // local storage for persisting progress and other state
    var store = new Store({key: 'townxelliot-media-player'});
    store.load();

    // the controller is responsible for wiring views to the
    // albumCollection model; it also persists the store and records
    // track progress data
    var controller = Controller(albumCollection, store);

    // connect the AudioFs implementation to the albumCollection,
    // so when files are added to/removed from/changed on the filesystem,
    // the albumCollection updates appropriately; it also adds
    // progress data for each audio file as it is added to the albumCollection,
    // by looking up the track in the store
    var bridge = AudioFsBridge({
      audioFs: audioFs,
      albumCollection: albumCollection,
      store: store
    });

    // fire the initial 'add' events from the audioFs;
    // which will in turn cause the albumCollection to be updated and fire
    // events; the albumCollection events will in turn
    // cause the UI to be populated
    bridge.init();

    // show the albums view
    controller.showAlbums();
  });
});
