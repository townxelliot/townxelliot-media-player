// a collection of Albums
define(
['tul', 'models/album', 'audio-fs'],
function (TUL, Album, AudioFs) {
  var sortFn = function (item1, item2) {
    if (item1.get('title') === AudioFs.UNKNOWN) {
      return 1;
    }
    else if (item2.get('title') === AudioFs.UNKNOWN) {
      return -1;
    }
    else if (item1.get('title') > item2.get('title')) {
      return 1;
    }
    else if (item2.get('title') > item1.get('title')) {
      return -1;
    }
    return 0;
  };

  var AlbumCollection = function () {
    if (!(this instanceof AlbumCollection)) {
      return new AlbumCollection();
    }

    TUL.ext(this, TUL.Collection({
      keyfield: 'id'
    }));
  };

  AlbumCollection.prototype.addAlbumRaw = function (opts) {
    this.update(Album(opts));
  };

  // returns the result of calling sortFn with the albums
  // with keys albumId1 and albumId2; can be used to determine the
  // ordering of two albums when you have their IDs
  AlbumCollection.prototype.compare = function (albumId1, albumId2) {
    return sortFn(this.get(albumId1), this.get(albumId2));
  };

  // find a track from all the albums in the collection by track ID
  AlbumCollection.prototype.findTrack = function (trackId) {
    var album = this.find(function (item) {
      return item.getTrack(trackId);
    });

    var track = null;

    if (album) {
      track = album.getTrack(trackId);
    }

    return track;
  };

  return AlbumCollection;
});
