// a single album, which is a collection of Tracks;
// the keys for the collection are track titles
define(['tul', 'models/track', 'audio-fs'], function (TUL, Track, AudioFs) {
  var sortFn = function (item1, item2) {
    var item1num = item1.get('tracknum') || 0;
    var item2num = item2.get('tracknum') || 0;
    return item1num - item2num;
  };

  var Album = function (opts) {
    if (!(this instanceof Album)) {
      return new Album(opts);
    }

    var self = this;

    opts = opts || {};
    opts.title = opts.title || AudioFs.UNKNOWN;

    TUL.ext(this, TUL.Model());

    this.set('id', TUL.norm(opts.title + '_' + opts.artist));

    this.on('change:title', function (data) {
      self.setPrettyTitle();
    });

    this.set('title', opts.title);
    this.setPrettyTitle();

    this.set('artist', opts.artist);

    var tracks = TUL.Collection({
      keyfield: 'id'
    });

    this.set('tracks', tracks);

    // total progress over all tracks
    this.set('progress', 0);
  };

  Album.prototype.setPrettyTitle = function () {
    var title = this.get('title');
    var prettyTitle = (title === AudioFs.UNKNOWN ? 'UNKNOWN ALBUM' : title);
    this.set('prettyTitle', prettyTitle);
  };

  Album.prototype.calculateProgress = function () {
    var progress = 0;

    this.get('tracks').forEach(function (track) {
      progress += track.get('progress');
    });

    // the album progress is the average of the progress across tracks
    var albumProgress = TUL.round(progress / this.get('tracks').asArray().length, 2);
    this.set('progress', albumProgress);
  };

  // add/update a track and set the progress % across all tracks
  Album.prototype.updateTrack = function (track) {
    this.get('tracks').update(track);

    // bind changes on the track to the calculateProgress() method
    track.off('change:progress', this.calculateProgress.bind(this));
    track.on('change:progress', this.calculateProgress.bind(this));

    this.calculateProgress();
  };

  Album.prototype.removeTrack = function (track) {
    this.get('tracks').remove(track.get('id'));
    track.off('change:progress', this.calculateProgress.bind(this));
    this.calculateProgress();
  };

  Album.prototype.getTrack = function (trackId) {
    return this.get('tracks').get(trackId);
  };

  Album.prototype.addTrackRaw = function (opts) {
    this.updateTrack(Track(opts));
  };

  return Album;
});
