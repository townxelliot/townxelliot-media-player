// a single track relating to an Album;
// each track corresponds to a single file on the filesystem,
// which will have a unique filename; hence the filename is also
// used as the ID for the track
define(['tul', 'audio-fs'], function (TUL, AudioFs) {
  var Track = function (opts) {
    if (!(this instanceof Track)) {
      return new Track(opts);
    }

    opts = opts || {};

    var self = this;

    TUL.ext(this, TUL.Model());

    opts.title = opts.title || AudioFs.UNKNOWN;

    this.on('change:title', function (data) {
      var prettyTitle = data.to;

      if (data.to === AudioFs.UNKNOWN) {
        prettyTitle = 'UNTITLED';
      }

      self.set('prettyTitle', prettyTitle);
    });

    this.set('id', TUL.norm(opts.filename));
    this.set('title', opts.title);
    this.set('artist', opts.artist);
    this.set('tracknum', opts.tracknum || 0);
    this.set('url', opts.url);
    this.set('album', opts.album);

    // percentage
    this.set('progress', 0);
  };

  return Track;
});
