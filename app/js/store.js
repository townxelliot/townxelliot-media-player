// local metadata storage which decorates data coming from
// the AudioFs (so this is agnostic of the AudioFs implementation);
// this is where we store progress data for each track and persist the
// state of the application
define([], function () {
  var Store = function (opts) {
    if (!(this instanceof Store)) {
      return new Store(opts);
    }

    opts = opts || {};

    if (!opts.key) {
      throw new Error('Store must be created with "key" option');
    }

    this.key = opts.key;

    // map of track ids to metadata, e.g.
    // { track1: {progress: 0.5}}
    this.tracks = {};

    // ID of last track played
    this.lastPlayed = null;
  };

  Store.prototype.loadProgress = function (track) {
    var progress = 0;

    var storedDataForTrack = this.tracks[track.get('id')];
    if (storedDataForTrack) {
      progress = storedDataForTrack.progress;
    }

    return progress;
  };

  Store.prototype.storeProgress = function (track) {
    var trackId = track.get('id');
    var storedDataForTrack = this.tracks[trackId];

    if (!storedDataForTrack) {
      this.tracks[trackId] = {progress: track.get('progress')};
    }
    else {
      this.tracks[trackId].progress = track.get('progress');
    }
  };

  Store.prototype.load = function () {
    var str = localStorage.getItem(this.key);

    if (str) {
      var data = JSON.parse(str);
      this.tracks = data.tracks;
      this.lastPlayed = data.lastPlayed;
    }
  };

  Store.prototype.save = function () {
    var data = JSON.stringify({
      tracks: this.tracks,
      lastPlayed: this.lastPlayed
    });

    localStorage.setItem(this.key, data);
  };

  Store.prototype.saveLastPlayed = function (track) {
    this.lastPlayed = track.get('id');
  };

  return Store;
});
