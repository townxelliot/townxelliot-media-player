// mixin with some useful methods for views; NB all views also
// have TUL.Ev mixed in, so they can fire events
define(['q', 'tul'], function (Q, TUL) {
  return function (wrapped) {
    TUL.defaults(wrapped, {
      elt: null,

      doInit: function () {
        return Q(this);
      },

      doRender: function () {
        return Q(this);
      },

      inited: false,
      ready: Q.defer(),

      childViews: {},

      addChildView: function (key, view) {
        this.childViews[key] = view;
      },

      removeChildView: function (key) {
        delete this.childViews[key];
      },

      removeAllChildViews: function () {
        this.childViews = {};
      },

      getChildView: function (key) {
        return this.childViews[key];
      },

      init: function () {
        if (!this.inited) {
          this.inited = true;

          var self = this;

          Q.fcall(this.doInit.bind(this))
          .done(
            function () {
              self.ready.resolve(self);
            },
            function (e) {
              console.error(e);
            }
          );
        }

        return this.ready.promise;
      },

      render: function () {
        var self = this;
        var dfd = Q.defer();

        this.init()
        .then(
          function () {
            return Q.fcall(self.doRender.bind(self));
          }
        ).done(
          function () {
            dfd.resolve(self);
          },

          function (e) {
            console.error(e);
          }
        );

        return dfd.promise;
      },

      // show the view; NB this relies on having an init() function
      // on the view which returns a promise (which resolves to the view)
      // and an elt property which represents the root DOM element
      // for the view
      show: function () {
        var self = this;
        var dfd = Q.defer();

        if (!this.elt) {
          dfd.resolve(this);
          return dfd.promise;
        }

        var self = this;

        this.init().then(
          function () {
            self.elt.setAttribute('data-active', true);
            dfd.resolve(self);
          }
        );

        return dfd.promise;
      },

      hide: function () {
        if (!this.elt) {
          return;
        }

        var self = this;

        this.init().then(
          function () {
            self.elt.setAttribute('data-active', false);
          }
        );
      },

      // return the DOM node for the string html as an array
      // or a single node if there is only one
      htmlToNodes: function (html) {
        var fake = document.createElement('div');
        fake.innerHTML = html;

        var childNodes = [];
        for (var i = 0; i < fake.children.length; i++) {
          childNodes.push(fake.children.item(i));
        }

        if (childNodes.length === 1) {
          return childNodes[0];
        }
        else {
          return childNodes;
        }
      },

      // append an array of DOM nodes to a parent element
      attachNodes: function (parentElt, nodes) {
        // ensure nodes is an array
        if (!TUL.isArray(nodes)) {
          nodes = [nodes];
        }

        TUL.forEach(nodes, function (node) {
          parentElt.appendChild(node);
        });
      },

      // attach the DOM nodes for the string html to a parent element
      attachHtml: function (parentElt, html) {
        var nodes = this.htmlToNodes(html);
        this.attachNodes(parentElt, nodes);
      },

      // set innerHTML for the view
      setHtml: function (str) {
        if (this.elt) {
          this.elt.innerHTML = str;
        }
      },

      // append view.elt to parent (default parent is this.elt);
      // if a selector sel is provided, the parent to append to is the
      // node selected by the selector, scoped to this.elt; or sel
      // can be a node to attach to
      appendView: function (view, sel) {
        var parent = this.elt;

        if (typeof sel === 'string') {
          parent = TUL.$(sel, this.elt);
        }
        else if (sel) {
          parent = sel;
        }

        if (!parent || parent.length === 0) {
          throw new Error('cannot append view, as no parent element is available');
        }

        if (TUL.isArray(parent)) {
          throw new Error('multiple possible parent elements returned by selector ' + sel);
        }

        if (!view.elt) {
          throw new Error('cannot append view, as child has no "elt" property');
        }

        parent.appendChild(view.elt);
      }
    }, TUL.Ev);
  };
});
