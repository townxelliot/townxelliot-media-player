define(
['q', 'tul', 'view', 'text!../../templates/album-brief.html'],
function (Q, TUL, View, template) {
  var AlbumBriefView = function (album) {
    if (!(this instanceof AlbumBriefView)) {
      return new AlbumBriefView(album);
    }

    this.elt = null;
    this.progressElt = null;

    this.id = album.get('id');

    this.album = album;
    this.album.on('change', this.albumChanged.bind(this));

    View(this);
  };

  // after rendering, find the progress element in the resulting HTML
  AlbumBriefView.prototype.setProgressElt = function () {
    this.progressElt = TUL.$('[data-role="slider-bar"]', this.elt);
  };

  // attach the view to the parent and do the render
  AlbumBriefView.prototype.doInit = function () {
    var self = this;
    var html = TUL.tpl(template, this.album);
    this.elt = this.htmlToNodes(html);
    this.setProgressElt();

    // add handlers for clicks on the title
    this.elt.addEventListener('click', function () {
      self.fire('action:showAlbum', self.album);
    });
  };

  // render the template for the album again, but only replace
  // the inner part of the <li>
  AlbumBriefView.prototype.doRender = function () {
    var html = TUL.tpl(template, this.album);
    var node = this.htmlToNodes(html);
    this.elt.innerHTML = node.innerHTML;
    this.setProgressElt();

    // this event causes the albums view to re-order itself
    this.fire('render');
  };

  AlbumBriefView.prototype.albumChanged = function (data) {
    // if progress changed, just update the progress bar
    if (data.prop === 'progress') {
      this.init().then(
        function (view) {
          view.progressElt.style.width = data.to + '%';
        }
      );
    }
    // otherwise update the whole render
    else {
      this.render();
    }
  };

  return AlbumBriefView;
});
