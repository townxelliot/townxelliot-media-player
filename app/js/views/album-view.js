// note that as tracks get added/removed from the album associated
// with an AlbumView, the AlbumView will re-render itself,
// as the album's progress value will change
define(
[
  'q',
  'gator',
  'tul',
  'view',
  'views/track-view',
  'text!../../templates/album.html'
],
function (Q, Gator, TUL, View, TrackView, template) {
  var AlbumView = function (elt) {
    if (!(this instanceof AlbumView)) {
      return new AlbumView(elt);
    }

    this.elt = elt;
    this.trackListElt = null;

    this.album = null;

    View(this);
  };

  AlbumView.prototype.doInit = function () {
    var self = this;

    Gator(this.elt).on('click', '[data-nav="albums"]', function () {
      self.fire('action:showAlbums');
    });
  };

  AlbumView.prototype.doRender = function () {
    if (this.album) {
      var self = this;

      var html = TUL.tpl(template, this.album);
      self.setHtml(html);

      this.trackListElt = TUL.$('[data-role="track-list"]', this.elt);

      return this.init()
      .then(
        function () {
          // create a sub-view for each track in the album
          var tracks = self.album.get('tracks');
          return self.addTrackViews(tracks);
        }
      )
      .done(this.sortTracks.bind(this));
    }
  };

  AlbumView.prototype.sortTracks = TUL.throttle(function () {
    var self = this;

    this.init().then(
      function (view) {
        var viewElts = TUL.$('[data-view="track"]', view.trackListElt);

        if (TUL.isArray(viewElts)) {
          var sortedViewElts = viewElts.sort(function (viewElt1, viewElt2) {
            var tracknum1 = viewElt1.getAttribute('data-tracknum');
            var tracknum2 = viewElt2.getAttribute('data-tracknum');
            return parseInt(tracknum1) - parseInt(tracknum2);
          });

          sortedViewElts.forEach(function (viewElt) {
            view.trackListElt.appendChild(viewElt);
          });
        }
      }
    );
  }, 10);

  AlbumView.prototype.addTrackView = function (track) {
    var self = this;

    return this.init()
    .then(
      function () {
        var trackView = TrackView(track);
        self.addChildView(trackView.id, trackView);

        // if a track number changes, sort the album view
        track.on('change:tracknum', function () {
          self.sortTracks();
        });

        // when this track view is clicked on, proxy its event up
        // (the controller manages it eventually)
        trackView.on('action:playTrack', function (track) {
          self.fire('action:playTrack', track);
        });

        return trackView.render();
      }
    )
    .then(
      function (trackView) {
        // insert the track view into the AlbumView
        self.trackListElt.appendChild(trackView.elt);

        return Q(self);
      }
    )
    .done(self.sortTracks.bind(self));
  };

  AlbumView.prototype.addTrackViews = function (tracks) {
    var self = this;
    var viewPromises = [];

    tracks.forEach(function (track) {
      // add a track view but don't sort
      viewPromises.push(self.addTrackView(track, false));
    });

    return Q.all(viewPromises).done(function () {
      // sort when all elements have been added
      return self.sortTracks();
    });
  };

  // highlight the currently playing track from this album
  AlbumView.prototype.highlightPlayingTrack = function (track) {
    // remove any highlights on existing track views and
    // highlight the view for the current track
    var trackId = track.get('id');

    TUL.forEach(this.childViews, function (view) {
      if (view.id === trackId) {
        view.highlight();
      }
      else {
        view.unhighlight();
      }
    });
  };

  // set the album to display in the view
  AlbumView.prototype.setAlbum = function (album) {
    if (album === this.album) {
      return;
    }

    this.removeAllChildViews();

    var self = this;

    var render = this.render.bind(this);

    var tracks;

    if (this.album) {
      tracks = this.album.get('tracks');
      tracks.off('add', render);
      tracks.off('change', render);
      tracks.off('remove', render);
    }

    this.album = album;

    tracks = this.album.get('tracks');
    tracks.on('add', render);
    tracks.on('change', render);
    tracks.on('remove', render);

    this.render();
  };

  return AlbumView;
});
