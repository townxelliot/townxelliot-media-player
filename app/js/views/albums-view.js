define(
['q', 'tul', 'view', 'views/album-brief-view', 'text!../../templates/albums.html'],
function (Q, TUL, View, AlbumBriefView, template) {
  // elt: the inside of this element will be replaced with the
  // rendered template
  var AlbumsView = function (elt, albumCollection) {
    if (!(this instanceof AlbumsView)) {
      return new AlbumsView(elt, albumCollection);
    }

    var self = this;

    this.elt = elt;
    this.albumListElt = null;

    this.albumCollection = albumCollection;

    // changes on the album collection add/remove sub-templates for albums
    this.albumCollection.on('add', this.albumAdded.bind(this));
    this.albumCollection.on('remove', this.albumRemoved.bind(this));

    View(this);
  };

  // attach the view to its element and do the render
  AlbumsView.prototype.doInit = function () {
    var self = this;

    var html = TUL.tpl(template, this.albumCollection);
    this.attachHtml(this.elt, html);
    this.albumListElt = TUL.$('[data-role="album-list"]', this.elt);

    // add a handler for the question mark button (which goes to the
    // last track played the previous time the app was used)
    var lastPlayedBtn = TUL.$('[data-role="last-played-btn"]', this.elt);
    lastPlayedBtn.addEventListener('click', function () {
      self.fire('action:playLastTrack');
    });
  };

  AlbumsView.prototype.sortAlbums = TUL.throttle(function () {
    var self = this;

    this.init().then(
      function (view) {
        var viewElts = TUL.$('[data-view="album-brief"]', view.albumListElt);

        if (TUL.isArray(viewElts)) {
          var sortedViewElts = viewElts.sort(function (viewElt1, viewElt2) {
            var albumId1 = viewElt1.getAttribute('data-album');
            var albumId2 = viewElt2.getAttribute('data-album');
            return self.albumCollection.compare(albumId1, albumId2);
          });

          sortedViewElts.forEach(function (viewElt) {
            view.albumListElt.appendChild(viewElt);
          });
        }
      }
    );
  }, 10);

  // insert a new AlbumBriefView for an album
  AlbumsView.prototype.albumAdded = function (data) {
    var self = this;
    var album = data.item;
    var albumBriefView = AlbumBriefView(album);

    albumBriefView.init();

    this.addChildView(albumBriefView.id, albumBriefView);

    // if this album brief view re-renders, reorder the list
    albumBriefView.on('render', this.sortAlbums.bind(this));

    // if this albumBriefView fires a nav event, propagate it up
    albumBriefView.on('action:showAlbum', function (album) {
      self.fire('action:showAlbum', album);
    });

    this.init().then(
      function (view) {
        self.appendView(albumBriefView, view.albumListElt);
        self.sortAlbums();
      }
    );
  };

  // remove the AlbumBriefView for an album and its corresponding element
  AlbumsView.prototype.albumRemoved = function (data) {
    var albumRemovedId = data.key;
    var albumBriefView = this.getChildView(albumRemovedId);
    if (albumBriefView) {
      albumBriefView.elt.remove();
      this.removeChildView(albumRemovedId);
    }
  };

  return AlbumsView;
});
