define(
['q', 'gator', 'tul', 'view', 'audio-player', 'text!../../templates/player.html'],
function (Q, Gator, TUL, View, AudioPlayer, template) {
 var PlayerView = function (elt) {
    if (!(this instanceof PlayerView)) {
      return new PlayerView(elt);
    }

    View(this);

    this.elt = elt;
    this.titleElt = null;

    this.hide();
    this.currentlyPlaying = null;

    this.audioPlayer = null;
  };

  PlayerView.prototype.onProgress = function (progress) {
    this.fire('progress', {track: this.currentlyPlaying, progress: progress});
  };

  PlayerView.prototype.onGoAlbum = function () {
    if (this.currentlyPlaying) {
      var album = this.currentlyPlaying.get('album');
      this.fire('action:showAlbum', album);
    }
  };

  PlayerView.prototype.doRender = function () {
    var self = this;
    var dfd = Q.defer();

    if (!this.audioPlayer) {
      var html = TUL.tpl(template, this.currentlyPlaying);
      this.setHtml(html);

      setTimeout(function () {
        var audioPlayerElt = TUL.$('[data-role="audio-player"]', self.elt);
        self.audioPlayer = AudioPlayer(audioPlayerElt);
        self.audioPlayer.on('progress', self.onProgress.bind(self));
        self.audioPlayer.on('goAlbum', self.onGoAlbum.bind(self));
        self.titleElt = TUL.$('[data-role="track-title"]', self.elt);
        dfd.resolve();
      }, 0);
    }
    else {
      // change the title HTML to the track num + title
      this.titleElt.innerHTML = this.currentlyPlaying.get('tracknum') + '. ' +
                                this.currentlyPlaying.get('prettyTitle');
      dfd.resolve();
    }

    return dfd.promise;
  };

  PlayerView.prototype.loadTrack = function (track) {
    var self = this;
    this.currentlyPlaying = track;

    this.render().then(function () {
      self.show();

      var onLoad = function (src) {
        self.audioPlayer.play();
        self.audioPlayer.off('loaded', this);
      };

      var onDuration = function () {
        var progress = track.get('progress');
        self.audioPlayer.seekToPercentage(progress);
        self.audioPlayer.off('duration', this);
      };

      self.audioPlayer.on('loaded', onLoad);
      self.audioPlayer.on('duration', onDuration);

      self.audioPlayer.load(track.get('url'));
    });
  };

  return PlayerView;
});
