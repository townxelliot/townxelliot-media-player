define(
['gator', 'tul', 'view', 'text!../../templates/track.html'],
function (Gator, TUL, View, template) {
  var TrackView = function (track) {
    if (!(this instanceof TrackView)) {
      return new TrackView(track);
    }

    View(this);

    this.elt = null;

    // the track view has a data-id set to the same id as the track
    this.id = track.get('id');

    this.track = track;
    this.track.on('change', this.render.bind(this));
  };

  TrackView.prototype.doInit = function () {
    var self = this;
    var html = TUL.tpl(template, this.track);
    this.elt = this.htmlToNodes(html);

    // clicking on this view requests that the track be played
    this.elt.addEventListener('click', function () {
      self.fire('action:playTrack', self.track);
    });
  };

  TrackView.prototype.doRender = function () {
    // reconstruct the inner HTML for the view
    var html = TUL.tpl(template, this.track);
    this.elt.innerHTML = this.htmlToNodes(html).innerHTML;
    this.fire('render');
  };

  TrackView.prototype.highlight = function () {
    this.elt.setAttribute('data-highlight', true);
  };

  TrackView.prototype.unhighlight = function () {
    this.elt.setAttribute('data-highlight', false);
  };

  return TrackView;
});
