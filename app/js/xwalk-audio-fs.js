define(['tul'], function (TUL) {
  // files returned passed to notifyAdd() should have this structure:
  /*
  {
    filename: 'a',
    url: audioFilesBase + '/vint.mp3',
    meta: {
      album: 'Best Of',
      title: 'Vint',
      artist: 'Spill Twins',
      tracknum: 1
    }
  }
  */
  return {
    init: function () {
      var self = this;
      var resp = audioFs.listFiles();
      TUL.forEach(resp.files, function (file) {
        self.notifyAdd(file);
      });
    }
  };
});
