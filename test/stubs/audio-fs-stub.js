define(['q', 'audio-fs', 'tul'], function (Q, AudioFs, TUL) {
  // this path is relative to the base path specified in
  // app/js/amd-config.js
  var audioFilesBase = require.toUrl('../../test/stubs/audio-files');
  audioFilesBase = audioFilesBase.replace(/\?.+/, '');

  var files = [
    {
      filename: 'a',
      url: audioFilesBase + '/vint.mp3',
      meta: {
        album: 'Best Of',
        title: 'Vint',
        artist: 'Spill Twins',
        tracknum: 1
      }
    },

    {
      filename: 'b',
      url: audioFilesBase + '/breeding_in_your_sponge.mp3',
      meta: {
        album: 'Best Of',
        title: 'Breeding in your Sponge',
        artist: 'Spill Twins',
        tracknum: 2
      }
    },

    {
      filename: 'c',
      url: audioFilesBase + '/hurrying_with_my_eye_born_night_wax.mp3',
      meta: {
        album: 'Best Of',
        title: 'Hurrying With My Eye-Born Night Wax',
        artist: 'Spill Twins',
        tracknum: 3
      }
    },

    {
      filename: 'd',
      url: audioFilesBase + '/umpet_steak_ripple.mp3',
      meta: {
        title: 'Umpet Steak Ripple',
        artist: 'Spill Twins'
      }
    },

    {
      filename: 'e',
      url: audioFilesBase + '/dogstep.mp3',
      meta: {
        title: 'Dogstep',
        artist: 'Spill Twins'
      }
    }
  ];

  return {
    init: function () {
      var self = this;

      TUL.forEach(files, function (file) {
        self.notifyAdd(file);
      });
    }
  };
});
