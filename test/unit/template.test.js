// node modules
var chai = require('chai');
var expect = chai.expect;
chai.should();

// replace node require with AMD require
require = require('../amd-require');

// load AMD modules under test
var Q = require('q');

describe('foo', function () {

  it('bar', function (done) {
    // chai's expect API
    expect(true).to.equal(true);

    // chai's should API
    'baz'.should.equal('baz');

    // dealing with async promise-returning code
    Q('boo').done(
      function (result) {
        result.should.equal('boo');
        done();
      },

      done
    );
  });

});
