package org.crosswalk.sample;

public class FileInfo {
  public String url;
  public String filename;
  public FileMeta meta;

  public FileInfo(String path, String title, String artist, String album, String tracknum) {
    this.url = path;
    this.filename = path;
    this.meta = new FileMeta(title, artist, album, tracknum);
  }
}
