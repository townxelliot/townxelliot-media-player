package org.crosswalk.sample;

public class FileMeta {
  public String title;
  public String artist;
  public String album;
  public String tracknum;

  public FileMeta(String title, String artist, String album, String tracknum) {
    this.title = title;
    this.artist = artist;
    this.album = album;
    this.tracknum = tracknum;
  }
}
